#include <Windows.h>
#include <Unknwn.h>

typedef HRESULT (__stdcall *DI8CR)(HINSTANCE hinst, DWORD dwVersion, GUID* riidltf, LPVOID* ppvOut,LPUNKNOWN punkOuter);

extern "C" __declspec(dllexport) HRESULT __stdcall DirectInput8Create(
	HINSTANCE hinst, DWORD dwVersion, GUID* riidltf, LPVOID* ppvOut, LPUNKNOWN punkOuter)
{
    // export this function undecorated without _ prefix
    #pragma comment(linker, "/EXPORT:DirectInput8Create=_DirectInput8Create@20")

	// get path to system directory
	char sysPath[MAX_PATH];
	GetSystemDirectoryA((LPSTR)sysPath, MAX_PATH);
    char dllPath[MAX_PATH];
    strcpy(dllPath, sysPath);
    strcat(dllPath, "\\DINPUT8.dll");

	// load DINPUT8.dll from system directory
	HMODULE hInput8Dll = LoadLibraryExA(dllPath, NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
	DI8CR fDirectInput8Create = (DI8CR)GetProcAddress(hInput8Dll, "DirectInput8Create");
	return fDirectInput8Create(hinst, dwVersion, riidltf, ppvOut, punkOuter);
}

/**
 * @brief patch instruction causing a crash related to emblems not being set
*/
void ApplyEmblemPatch()
{
    // get address to fault instruction
    char* emblemPatchAddr = (char*)GetModuleHandleA(nullptr) + 0xB397A6;

    // override instruction with NOPs
    char patches[4] = { static_cast<char>(0x90), static_cast<char>(0x90), static_cast<char>(0x90), static_cast<char>(0x90) };
    DWORD oldProtect = 0;
    VirtualProtect((LPVOID)emblemPatchAddr, sizeof(patches), PAGE_READWRITE, &oldProtect);
    memcpy((char*)emblemPatchAddr, &patches, sizeof(patches));
    VirtualProtect((LPVOID)emblemPatchAddr, sizeof(patches), oldProtect, &oldProtect);
}

void LoadBlrevive()
{
    HMODULE hBlreviveDll = LoadLibraryA("BLRevive.dll");
}


BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        ApplyEmblemPatch();
        LoadBlrevive();
        break;
    case DLL_THREAD_ATTACH:
		break;
    case DLL_THREAD_DETACH:
		break;
    case DLL_PROCESS_DETACH:
        break;
    }

    return TRUE;
}