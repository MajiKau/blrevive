#pragma once
#include <string>
#include <BLRevive/Log.h>

namespace BLRE
{
	class Component
	{
	public:
		Component(std::string name);
		Component(std::string name, std::string logFile);

	protected:
		LogFactory::TSharedLogger Log;

	protected:
		std::string ComponentName;
		std::string ComponentLogFile;
	};
}