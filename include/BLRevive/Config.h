#pragma once
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <string>
#include <vector>
#include <BLRevive/Utils.h>
#include <BLRevive/Log.h>
#include <BLRevive/Utils/URL.h>

namespace BLRE
{
    struct Config
    {
        struct logger_t {
            LogLevel Level = LogLevel::none;
            LoggerTarget Target = LoggerTarget::FILE;
            std::string FilePath = "blrevive-{server}{timestamp}.log";
        } Logger;

        struct console_t {
            bool Enable = true;
            std::vector<std::string> CmdWhitelist = {};
            std::vector<std::string> CmdBlacklist = {};
        } Console;

        struct server_t {
            bool Enable = true;
            bool AuthenticateUsers = true;
        } Server;

        std::map<std::string, nlohmann::json> Modules;

        static Config Load(std::string path, bool createDefault = false);
        void Save(std::string path);

        template<typename TMod>
        TMod GetModuleConfig()
        {
            return Modules[TMod::MOD_NAME].get<TMod::TConfig>();
        }

        template<typename TMod>
        void SaveModuleConfig(TMod::TConfig& conf)
        {
            Modules[TMod::MOD_NAME] = conf;
        }
    };

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Config::logger_t,
        Level, Target, FilePath);

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Config::console_t,
        Enable, CmdWhitelist, CmdBlacklist);

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Config::server_t,
        Enable, AuthenticateUsers);

    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Config,
        Logger, Console, Modules, Server);

#define MAP_URL_CONFIG_OPT(ns, key) \
    conf.##key = url.GetParam(#ns "." #key, conf.##key);

#define MAP_URL_CONFIG(type, ns, ...) \
    inline void map_url_config(type& conf, BLRE::Utils::URL url) { \
        FOR_EACH(MAP_URL_CONFIG_OPT, ns, __VA_ARGS__); \
    }

#define MAP_JSON_URL_CONFIG(type, ns, ...) \
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(type, __VA_ARGS__); \
    MAP_URL_CONFIG(type, ns, __VA_ARGS__);

    MAP_URL_CONFIG(Config, blre, 
        Console.Enable, 
        Logger.Target, Logger.Level, Logger.FilePath,
        Server.Enable, Server.AuthenticateUsers);

}