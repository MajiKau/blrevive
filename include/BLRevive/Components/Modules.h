#pragma once

#include <BLRevive/BLRevive.h>
#include <BLRevive/Component.h>
#include <nlohmann/json.hpp>
#include <semver.hpp>

namespace semver {
	void from_json(const nlohmann::json& j, version& v);
	void to_json(nlohmann::json& j, const version& v);
}

namespace BLRE::Modules
{
	/**
	 * Interface for module classes.
	*/
	class BLReviveModule : public virtual Component
	{
	public:
		virtual void Initialize() = 0;
		virtual std::string GetName() = 0;
	};

	// initializing function for modules
	typedef BLReviveModule* (*ModuleInitializer)(BLRevive* BLRevive);


	/**
	 * Information about a module
	 */
	struct ModuleInfo
	{
		std::string Name = "";
		semver::version Version;
		std::map<std::string, std::string> Requires = {};

		ModuleInitializer Initializer = nullptr;
		HINSTANCE DllInstance = nullptr;
		std::string FileName = "";
		BLReviveModule* Instance = nullptr;
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ModuleInfo,
		Name, Version, Requires);


    /**
     * Manager class for handling modules
    */
    class Manager : public Component
	{
	public:
		Manager();

		/**
		 * Load all modules defined in config
		 */
		bool LoadAllModulesFromConfig();

		/**
		 * Load a single module
		 *
		 * @param  FileName	module filename
		 */
		bool LoadModule(std::string FileName);

		bool AreModuleDependenciesMatched(ModuleInfo& info);
	protected:
		ModuleInfo BlreInfo = {};
		// current loaded modules
		std::map<std::string, ModuleInfo> LoadedModules = {};
		// module initializers
		std::map<std::string, ModuleInitializer> Initializers = {};
	};
}