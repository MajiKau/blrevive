#pragma once
#include <BLRevive/Detours/DetourBase.h>
#include <SdkHeaders.h>

namespace BLRE::Detours
{
	typedef void(__thiscall* TProcessEvent)(class UObject*, class UFunction*, void*, void*);

	/**
	 * Helper class for detouring ProcessEvent.
	 *
	 * @attention Needs to be compiled with optimization on (/O2), will throw stack cookie exception else then.
	*/
	class ProcessEvent : public DetourBase
	{
	public:
		/**
		 * Detour ProcessEvent
		 *
		 * @return detour attached
		*/
		bool Attach() override;

		/**
		 * Remove ProcessEvent detour
		 * @return detour removed
		*/
		bool Detach() override;

		ProcessEvent();
	};
}
