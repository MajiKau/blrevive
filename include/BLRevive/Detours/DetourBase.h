#pragma once
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <BLRevive/Component.h>
#include <BLRevive/EventLog.h>

namespace BLRE::Detours
{
	/**
	 * Abstract base for detouring functions using MS detours.
	*/
	class DetourBase : public Component
	{
	public:
		inline static BLRE::EventLog* eventLog = nullptr;

		/**
		 * Create detour of Original function to Detour
		 *
		 * @return detour attached
		*/
		virtual bool Attach();

		/**
		 * Reset detour
		 *
		 * @return detour reset
		*/
		virtual bool Detach();

		/**
		 * Wether detour is attached
		 *
		 * @return detour attached
		*/
		bool IsDetoured()
		{
			return _IsDetoured;
		}

		DetourBase(uintptr_t offset, PVOID detour, std::string Name);
	protected:

		~DetourBase()
		{
			Detach();
		}

		PVOID OrigAddr;
		PVOID* Original;
		PVOID Detour;
		bool _IsDetoured;
	};
}