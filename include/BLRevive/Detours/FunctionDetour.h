#pragma once
#include <SdkHeaders.h>
#include <type_traits>
#include <queue>
#include <BLRevive/Component.h>
#include <BLRevive/Log.h>

namespace BLRE::Detours
{
	/**
	 * @brief Base class for function detours.
	*/
	class FunctionDetour : public Component
	{
	public:
		// function to detour
		UFunction* Function;

		// current stack
		FFrame* Stack;
		// current result
		void* Result;
		// current object
		UObject* Object;

		static inline BLRE::LogFactory::TSharedLogger StaticLog = nullptr;

	protected:
		FunctionDetour(UFunction* Function);
		~FunctionDetour();

		// current detour queue from function call
		std::queue<FunctionDetour*> detourQueue;
		// wether to skip the current function call
		bool skipFunction;
		// wether function was executed or skipped
		bool continued;

	public:

		/**
		 * @brief Get UFunction insance by its full name
		 * @param  FunctionName full name of the function
		 * @return instance of UFunction
		*/
		static UFunction* GetUFunctionByName(std::string FunctionName);

		/**
		 * @brief Create a function detour to a lose function by function pointer
		 * @tparam R return type of the function (must match original return type)
		 * @tparam ...Args arguments of the function (must match original arguments)
		 * @param Function function instance to detour
		 * @param Detour function to call on detour
		 * @return FunctionDetourFunc
		*/
		template<typename R, class ...Args>
		static FunctionDetour* Create(UFunction* Function, R(*Detour)(FunctionDetour*, Args...));

		/**
		 * @brief Create a function detour to a lose function by function name
		 * @tparam R return type of the function (must match original return type)
		 * @tparam ...Args arguments of the function (must match original arguments)
		 * @param FunctionName full name of function to detour
		 * @param Detour function to call on detour
		 * @return FunctionDetourFunc
		*/
		template<typename R, class ...Args>
		static FunctionDetour* Create(std::string FunctionName, R(*Detour)(FunctionDetour*, Args...));

		/**
		 * @brief Create a function detour to a member function of a non-owning class by function instance
		 * @tparam TReturn return type of the function
		 * @tparam TDetourArg argument of the function (either FFrame or param struct)
		 * @tparam TDetourClass class which owns the detour function
		 * @tparam TTargetClass class which owns the original function
		 * @param Function instance of function to detour
		 * @param DetourClass instance of owner of detour function
		 * @param Detour member function to call on detour
		 * @return FunctionDetourCls
		*/
		template<typename TDetourClass, typename TTargetClass, typename TReturn, typename TDetourArg>
		static FunctionDetour* Create(UFunction* Function, TDetourClass* DetourClass, TReturn(TDetourClass::* Detour)(FunctionDetour*, TTargetClass*, TDetourArg));

		/**
		 * @brief Create a function detour to a member function of a non-owning class by function name
		 * @tparam TReturn return type of the function
		 * @tparam TDetourArg argument of the function (either FFrame or param struct)
		 * @tparam TDetourClass class which owns the detour function
		 * @tparam TTargetClass class which owns the original function
		 * @param FunctionName full name of function to detour
		 * @param DetourClass instance of owner of detour function
		 * @param Detour member function to call on detour
		 * @return FunctionDetourCls
		*/
		template<typename TDetourClass, typename TTargetClass, typename TReturn, typename TDetourArg>
		static FunctionDetour* Create(std::string FunctionName, TDetourClass* DetourClass, TReturn(TDetourClass::* Detour)(FunctionDetour*, TTargetClass*, TDetourArg));

		/**
		 * @brief Create a function detour to a member function of an owning class by function instance
		 * @tparam TDetourClass class which owns the detour function
		 * @tparam TReturn return type of detour function
		 * @tparam TDetourArg argument of detour function (either FFrame or param struct)
		 * @param Function instance of function to detour
		 * @param Detour member function to call on detour
		 * @return FunctionDetourObject
		*/
		template<typename TDetourClass, typename TReturn, typename TDetourArg>
		static FunctionDetour* Create(UFunction* Function, TReturn(TDetourClass::* Detour)(FunctionDetour*, TDetourArg));

		/**
		 * @brief Create a function detour to a member function of an owning class by function name
		 * @tparam TDetourClass class which owns the detour function
		 * @tparam TReturn return type of detour function
		 * @tparam TDetourArg argument of detour function (either FFrame or param struct)
		 * @param FunctionName full name of function to detour
		 * @param Detour member function to call on detour
		 * @return FunctionDetourObject
		*/
		template<typename TDetourClass, typename TReturn, typename TDetourArg>
		static FunctionDetour* Create(std::string FunctionName, TReturn(TDetourClass::* Detour)(FunctionDetour*, TDetourArg));

		void Enable();
		void Disable();


		/**
		 * @brief Continue execution of the original function
		 * @param This context object
		 * @param Stack stack
		 * @param Result context result
		*/
		void Continue(bool forceSkipFunction = false);

		/**
		 * @brief Skip execution of the original function
		 * @param Stack stack
		*/
		void Skip();

		/**
		 * @brief Call the detour wrapper which will invoke the detour target
		 * @param This context object
		 * @param Stack stack
		 * @param Result context result
		*/
		virtual void Call(UObject* This, FFrame& Stack, void* Result, std::queue<FunctionDetour*> queue, bool skipFunction = false);

	protected:
		void ContidtionalContinue()
		{
			if (!this->continued)
				this->Continue();

			this->continued = false;
		}
	};
}

#include <BLRevive/Detours/FunctionDetourImpl.h>