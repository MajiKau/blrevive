#pragma once
#include <string>
#include <windows.h>
#include <nlohmann/json.hpp>
#include <fstream>
#include <spdlog/fmt/fmt.h>
#include <BLRevive/Errors.h>

namespace BLRE::Utils
{
	/**
	 * Wether current instance is running server
	 *
	 * @return is server
	 */
	inline bool IsServer()
	{
		std::string cli(GetCommandLineA());
		return cli.find(" server ") != std::string::npos;
	}

	template<typename TData>
	void WriteJsonFile(std::string path, TData data, int indention = 4)
	{
		std::ofstream o(path);
		if(!o.good())
			throw file_no_access(path);

		nlohmann::json j = data;
		o << std::setw(indention) << j << std::endl;
		o.close();
	}

	template<typename TData = nlohmann::json>
	void ReadJsonFile(std::string path, TData& data)
	{
		if(!std::filesystem::exists(path))
			throw file_not_found(path);

		std::ifstream i(path);
		if (!i.good())
			throw file_no_access(path);

		nlohmann::json j = nlohmann::json::parse(i);
		data = j.get<TData>();
		i.close();
	}

	template<typename TData = nlohmann::json>
	const TData ReadJsonFile(std::string path)
	{
		TData data;
		ReadJsonFile(path, data);
		return data;
	}

	namespace FS
	{
		/**
		 * Get current work directory
		 *
		 * @return cwd
		 */
		std::string CWD();

		/**
		 * Get base path of blacklight (for eg "C:\Program Files (x86)\Steam\steamapps\common\blacklightretribution\")
		 *
		 * @return blr base path
		 */
		std::string BlrBasePath();

		/**
		 * Get config path
		 *
		 * @return config path
		 */
		static std::string BlreviveConfigPath() { return BlrBasePath() + "FoxGame\\Config\\BLRevive\\"; }

		/**
		 * Get log path
		 *
		 * @return log path
		 */
		static std::string BlreviveLogPath() { return BlrBasePath() + "FoxGame\\Logs\\"; }

		/**
		 * Get module path
		 *
		 * @return module path
		 */
		static std::string BlreviveModulePath() { return BlrBasePath() + "Binaries\\Win32\\Modules\\"; }

		static std::filesystem::path Relative(std::filesystem::path absPath) { return std::filesystem::relative(absPath, BlrBasePath()); }
		static std::string Relative(std::string abs) { return std::filesystem::relative(std::filesystem::path(abs), std::filesystem::path(BlrBasePath())).string(); }
	}
}

// macro utils for variadic macro functions
#define INDIRECT_EXPAND(m, args) m args

#define CONCATE_(X,Y) X##Y
#define CONCATE(X,Y) CONCATE_(X,Y)
#define UNIQUE(NAME) CONCATE(NAME, __LINE__)

#define NUM_ARGS_2(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, TOTAL, ...) TOTAL
#define NUM_ARGS_(...) INDIRECT_EXPAND(NUM_ARGS_2, (__VA_ARGS__))
#define NUM_ARGS(...) NUM_ARGS_(__VA_ARGS__, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define VA_MACRO(MACRO, ...) INDIRECT_EXPAND(CONCATE, (MACRO, NUM_ARGS(__VA_ARGS__)))(__VA_ARGS__)

#define EXPAND( x ) x

#define FE_0(WHAT)
#define FE_1(WHAT, ns, X) EXPAND(WHAT(ns, X))
#define FE_2(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_1(WHAT, ns, __VA_ARGS__))
#define FE_3(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_2(WHAT, ns, __VA_ARGS__))
#define FE_4(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_3(WHAT, ns, __VA_ARGS__))
#define FE_5(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_4(WHAT, ns, __VA_ARGS__))
#define FE_6(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_5(WHAT, ns, __VA_ARGS__))
#define FE_7(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_6(WHAT, ns, __VA_ARGS__))
#define FE_8(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_7(WHAT, ns, __VA_ARGS__))
#define FE_9(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_8(WHAT, ns, __VA_ARGS__))
#define FE_10(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_9(WHAT, ns, __VA_ARGS__))
#define FE_11(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_10(WHAT, ns, __VA_ARGS__))
#define FE_12(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_11(WHAT, ns, __VA_ARGS__))
#define FE_13(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_12(WHAT, ns, __VA_ARGS__))
#define FE_14(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_13(WHAT, ns, __VA_ARGS__))
#define FE_15(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_14(WHAT, ns, __VA_ARGS__))
#define FE_16(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_15(WHAT, ns, __VA_ARGS__))
#define FE_17(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_16(WHAT, ns, __VA_ARGS__))
#define FE_18(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_17(WHAT, ns, __VA_ARGS__))
#define FE_19(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_18(WHAT, ns, __VA_ARGS__))
#define FE_20(WHAT, ns, X, ...) WHAT(ns, X)EXPAND(FE_19(WHAT, ns, __VA_ARGS__))


#define GET_MACRO(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,NAME,...) NAME 
#define FOR_EACH(action,ns,...) \
  EXPAND(GET_MACRO(_0,__VA_ARGS__,FE_20,FE_19,FE_18,FE_17,FE_16,FE_15,FE_14,FE_13,FE_12,FE_11,FE_10,FE_9,FE_8,FE_7,FE_6,FE_5,FE_4,FE_3,FE_2,FE_1,FE_0)(action, ns, __VA_ARGS__))
