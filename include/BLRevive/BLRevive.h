#pragma once

#include <BLRevive/Log.h>
#include <BLRevive/Config.h>
#include <BLRevive/Components/Console.h>
#include <BLRevive/Detours/DetourBase.h>
#include <BLRevive/Detours/DetourRegistry.h>
#include <BLRevive/Utils/URL.h>

#ifdef DEBUG
#include <BLRevive/Detours/ProcessEvent.h>
#include <BLRevive/Detours/CallFunction.h>
#endif

#define MainLog BLRE::BLRevive::GetInstance()->Log

namespace BLRE
{
    namespace Modules
    {
        class Manager;
    };

    class Patcher;

    /**
     * Main class for the project handling all components and initializations.
    */
    class BLRevive
    {
    public:
        /**
         * Create new instance.
         * 
         * @return BLRevive instance
        */
        static BLRevive* Create();

        /**
         * Get instance.
         * 
         * @return BLRevive instance
        */
        static BLRevive* GetInstance();

        /**
         * Initialize BLRevive
        */
        void Initialize();

        // log factory
        LogFactory* LogFactory = nullptr;
        
        // logger for BLRevive component
        LogFactory::TSharedLogger Log = nullptr;

        // config
        Config Config = {};

        // URL
        Utils::URL URL;

        // patcher
        Patcher* Patcher;
        // console manager
        UI::Console* Console = nullptr;
        // module manager
        Modules::Manager* ModuleMgr = nullptr;
        // detour registry
        Detours::FunctionDetourRegistry* Detours = nullptr;
        
#ifdef DEBUG
        // process event detour manager (only for debug)
        Detours::ProcessEvent* PeDetour = nullptr;
        // call function detour manager (only for debug)
        Detours::CallFunction* CfDetour = nullptr;
#endif
    protected:
        BLRevive();
        ~BLRevive();

        // instance of BLRevive
        inline static BLRevive* _Instance = nullptr;
    };
}