#pragma once
#include <Windows.h>
#include <detours.h>
#include <fstream>
#include <nlohmann/json.hpp>
#include <spdlog/fmt/fmt.h>
#include <BLRevive/InputBox.h>
#include <BLRevive/Utils.h>
#include <BLRevive/Config.h>

namespace BLRE::DebugUtils
{
	struct DebugConfig
	{
		struct SServer
		{
			std::string URL = "HeloDeck?NumBots=10";
			bool AttachClient = false;
			bool LogProcessEvent = false;
			bool LogCallFunction = false;
		} Server;

		struct SClient
		{
			std::string URL = "127.0.0.1?Name=AwesomeDev";
			bool StartServer = true;
			bool LogProcessEvent = false;
			bool LogCallFunction = false;
		} Client;
	};

	/**
	 * Override default command line string to set custom parameters before app starts.
	*/
	void InjectCustomCommandLine();

	/**
	 * Get debugging configuration from <ConfigPath>/debug.json.
	 * 
	 * @return debug config
	*/
	DebugConfig GetDebugConfig();

	/**
	 * Create custom command line from user input.
	*/
	void GetUserCmd();

	/**
	 * Callback for Kernel32::GetCommandLineW to override the default command line.
	 * 
	 * @return custom command line
	*/
	LPWSTR HkGetCommandLineW();

	void* StartCompanionProcess();

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(DebugConfig::SServer, URL, AttachClient, LogProcessEvent, LogCallFunction);
	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(DebugConfig::SClient, URL, StartServer, LogProcessEvent, LogCallFunction);
	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(DebugConfig, Server, Client);
}