#include <BLRevive/Utils.h>

using namespace BLRE;

std::string Utils::FS::CWD()
{
	static std::string name;
	
	if (name.empty())
	{
		char buffer[MAX_PATH] = { 0 };
		GetModuleFileNameA(NULL, buffer, MAX_PATH);
		std::string::size_type pos = std::string(buffer).find_last_of("\\");
		name = std::string(buffer).substr(0, pos);
	}

	return name;
}

std::string Utils::FS::BlrBasePath()
{
	static std::string _basePath;
	if (_basePath.empty())
		_basePath = CWD().substr(0, CWD().find("Binaries\\"));

	return _basePath;
}
