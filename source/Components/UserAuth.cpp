#include <BLRevive/Components/UserAuth.h>
#include <BLRevive/Utils.h>
#include <BLRevive/BLRevive.h>
#include <BLRevive/Detours/FunctionDetour.h>

using namespace BLRE;


typedef bool(*tSteamAPI_Init)();
static BLRE::Detours::DetourBase* dtSteamApiInit = nullptr;
static tSteamAPI_Init origSteamAPI_Init = nullptr;

bool hkSteamAPI_Init()
{
	// disable SteamAPI_Init in favor of SteamGameServer_Init
	return false;
}

void UserAuth::InitializeSteamApi()
{
	auto blre = BLRE::BLRevive::GetInstance();
	if (Utils::IsServer() && blre->Config.Server.AuthenticateUsers)
	{
		// hook SteamAPI_Init to disable it on server side and use bundled steamclient.dll instead
		HMODULE hSteamAPI = GetModuleHandleA("steam_api.dll");
		if (hSteamAPI == nullptr)
		{
			MainLog->error("Failed to get handle for steam_api.dll!");
			return;
		}

		origSteamAPI_Init = (tSteamAPI_Init)GetProcAddress(hSteamAPI, "SteamAPI_Init");
		if (origSteamAPI_Init == nullptr)
		{
			MainLog->error("Failed to get ProcAddress of steam_api.dll#SteamAPI_Init");
			return;
		}

		dtSteamApiInit = new BLRE::Detours::DetourBase((uintptr_t)origSteamAPI_Init, hkSteamAPI_Init, "SteamAPI_Init");
		dtSteamApiInit->Attach();
	}
}

void UserAuth::SetupBanList(AAccessControl* ac)
{
	std::vector<uint64_t> banlist;
	// load banlist from json
	try {
		Utils::ReadJsonFile("banlist.json", banlist);
	} catch (file_not_found& ex) {
		Log->error("banlist.json was not found");
	} catch (nlohmann::json::exception& ex) {
		Log->error("failed to parse json from banlist.json");
	}

	// propagate ids to AAccessControl::BannedIDs
	for (uint64_t bannedId : banlist) {
		FUniqueNetId netId(bannedId);
		ac->BannedIDs.push_back(netId);
	}
}

void UserAuth::Init()
{
	if (!BLRE::BLRevive::GetInstance()->Config.Server.AuthenticateUsers)
		return;

	Log->debug("Initializing UserAuth component");
	auto blre = BLRevive::GetInstance();

	if (BLRE::Utils::IsServer())
	{
		// loading the steamclient and steam interface needed for servers without steam client installed
		HMODULE hSteam = LoadLibraryA("Steam.dll");
		if (hSteam == nullptr)
		{
			Log->error("Failed to get handle of Steam.dll");
			return;
		}

		HMODULE hSteamClient = LoadLibraryA("steamclient.dll");
		if (hSteamClient == nullptr)
		{
			Log->error("Failed to get handle of steamclient.dll");
			return;
		}

		// initialize the gameserver
		bool isInit = SteamGameServer_Init(0x7f000001, 7779, 7777, 7780, EServerMode::eServerModeNoAuthentication, "3.02");
		if (!isInit)
		{
			Log->error("Failed to initialize steam game server (SteamGameServer_Init returned false)!");
			return;
		}

		if (!SteamGameServer())
		{
			Log->error("failed to retrieve steam game server for unkown reasons");
			return;
		}

		// wait for ServerAuthenticateTicket function to be initialized
		while (UObject::GObjObjects()->Count < 56798 || UObject::GObjObjects()->at(56798) == nullptr || ((UFunction*)UObject::GObjObjects()->at(56798))->Func == nullptr)
			Sleep(100);

		// detour ServerAuthenticateTicket to validate steam tickets
		auto dtServerAuthenticateTicket = Detours::FunctionDetour::Create("FoxGame.FoxPC.ServerAuthenticateTicket", this, &UserAuth::ServerAuthenticateTicket);
		dtServerAuthenticateTicket->Enable();

		dtServerNotifyPCSpawned = Detours::FunctionDetour::Create("FoxGame.FoxPC.ServerNotifyPCSpawned", this, &UserAuth::ServerNotifyPCSpawned);
		dtServerNotifyPCSpawned->Enable();

		// detour AAccessControl::PostBeginPlay to propagate ban list
		auto dtAccessControlPostBeginPlay = Detours::FunctionDetour::Create("Engine.AccessControl.PostBeginPlay", this, &UserAuth::AAccessControl_PostBeginPlay);
		dtAccessControlPostBeginPlay->Enable();

		// enable steam gameserver callbacks
		m_SteamCallbackThread = std::thread(&UserAuth::SteamCallbackThread, this);
	}
}

void UserAuth::SteamCallbackThread()
{
	while (true)
	{
		Sleep(100);
		SteamGameServer_RunCallbacks();
	}
}

void DETOUR_CB_CLS_IMPL(UserAuth::ServerAuthenticateTicket, AFoxPC, ServerAuthenticateTicket, pc)
{
	// deserialize ticket data (needed because FString replication is serialized)
	auto sticket = UnserializeNetString(params->ticketData);

	// get steam id from params
	//uint64_t iSteamId = (uint64_t)params->steamID.B << 32 | params->steamID.A;
	auto iSteamId = params->steamID.ToUint64();
	auto steamid = CSteamID(iSteamId);

	Log->info("Authenticating steam ticket for {} ({})", pc->PlayerReplicationInfo->PlayerName, iSteamId);

	auto steamGameServer = SteamGameServer();
	if (!steamGameServer) {
		Log->error("failed to retrieve steam game server");
		return;
	}

	// validate ticket with steam api
	auto result = steamGameServer->BeginAuthSession(sticket, params->ticketSize, steamid);
	auto accessControl = UObject::GetInstanceOf<AAccessControl>();

	// kick player if steam session ticket is invalid
	if (result != EBeginAuthSessionResult::k_EBeginAuthSessionResultOK && result != EBeginAuthSessionResult::k_EBeginAuthSessionResultGameMismatch)
	{
		accessControl->ForceKickPlayer(pc, "Steam authentication failed!");
		Log->info("Kicked player {} ({}) because of invalid session ticket {}", pc->PlayerReplicationInfo->PlayerName, iSteamId, result);
	}

	// kick player if steam id was banned on this server
	for (auto bannedId : accessControl->BannedIDs)
	{
		if (bannedId.Uid.A == params->steamID.A && bannedId.Uid.B == params->steamID.B)
		{
			accessControl->ForceKickPlayer(pc, "You have been banned on this server!");
			Log->info("Kicked player {} ({}) because was banned", pc->PlayerReplicationInfo->PlayerName, iSteamId);
		}
	}
}

void DETOUR_CB_CLS_IMPL(UserAuth::AAccessControl_PostBeginPlay, AAccessControl, PostBeginPlay, accessControl)
{
	// cache AccessControl
	this->accessControl = accessControl;
	// propagate ban list
	this->SetupBanList(accessControl);
}

void DETOUR_CB_CLS_IMPL(UserAuth::ServerNotifyPCSpawned, AFoxPC, ServerNotifyPCSpawned, pc)
{
	auto steamId = pc->PlayerReplicationInfo->SecondaryId.Uid.ToUint64();

	// check if SteamID is zero (= steam not running/not logged in)
	if (steamId == 0)
	{
		// kick player if SteamID is zero
		Log->info("Player {} (PlayerID={}) is kicked because of missing SteamID", pc->PlayerReplicationInfo->PlayerName, pc->PlayerReplicationInfo->PlayerID);
		accessControl->ForceKickPlayer(pc, "Missing SteamID");
		return;
	}

	this->dtServerNotifyPCSpawned->Continue();
}

const char* UserAuth::UnserializeNetString(FString string)
{
	char* uns = (char*)calloc(1, string.Count);

	if (uns != nullptr)
		for (int i = 0; i < string.Count; i++)
			uns[i] = string.Data[i] - 0xA;

	return uns;
}