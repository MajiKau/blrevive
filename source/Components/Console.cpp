#include <Windows.h>
#include <iostream>
#include <BLRevive/Components/Console.h>
#include <BLRevive/Components/Menu.h>

BLRE::UI::Console::Console()
	: Component("Console")
{
	Root = new Menu();
}

void BLRE::UI::Console::AttachOrCreate()
{
	if (!AttachConsole(-1)) {
		if (!FreeConsole()) {
			MessageBoxA(nullptr, "Failed to free", "Free", MB_OK);
			auto freeErrorCode = GetLastError();
			return;
		}

		if (!AllocConsole()) {
			MessageBoxA(nullptr, "Failed to alloc", "Alloc", MB_OK);
			auto allocErrorCode = GetLastError();
			return;
		}
	}

	FILE* fDummy;
	freopen_s(&fDummy, "CONOUT$", "w", stdout);
	freopen_s(&fDummy, "CONOUT$", "w", stderr);
	freopen_s(&fDummy, "CONIN$", "r", stdin);
}

void BLRE::UI::Console::Init()
{
	Log->debug("Initializing Console");
	// allocate new console
	if (!FreeConsole()) {
		Log->error("FreeConsole Error: {0}", GetLastError());
		return;
	}

	if (!AllocConsole()) {
		Log->error("AllocConsole Error: {0}", GetLastError());
		return;
	}

	FILE* fDummy;
	freopen_s(&fDummy, "CONOUT$", "w", stdout);
	freopen_s(&fDummy, "CONOUT$", "w", stderr);
	freopen_s(&fDummy, "CONIN$", "r", stdin);
	Log->debug("Console initialized!");
}

void BLRE::UI::Console::Run()
{
	while (!shouldExit) {
		std::cout << "> ";

		std::string cmd;
		while(cmd == "")
			std::getline(std::cin, cmd);
		std::cout << "cmd: " << cmd << std::endl;

		Root->Exec(cmd);
	}
}

void BLRE::UI::Console::Exit()
{
	shouldExit = true;
}