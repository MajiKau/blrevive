# Basics

## BLRevive API

The `BLRE::BLRevive` class provides a simple interface to access all the components and interfaces for this project and is used to share them with modules.

It should be treated as singleton, meaning there must be only one instance for a proccess. The instance is created on startup and can be retrieved with `BLRevive::GetInstance()` within the scope of this library.

A pointer to the instance is passed to modules through the first parameter of the initializor function.

## Logging

Most of the logging features are similar/equal to the ones of [spdlog] which is the underlying framework in use.

Look into the [Logging](Logging.md) docs for more info.

## Configuration

The `BLRE::Config` class provides access to json configuration files.

Configuration files are located inside the `<BlacklightDirectory>/FoxGame/Config/BLRevive` directory.

The configuration file that should be used can be passed as URL parameter when starting the BL:R instance (eg: `BLRevive.exe server HeloDeck?Config=MyCustomConfig` will load the `MyCustomConfig.json` from the configuration directory).

When the Config URL parameter is not set, it defaults to `default`.

### sections

Configuration is split into sections (`BLRE::Config::ConfigSection`) which refer to a specific object within the json files. Each top-level object of a json configuration file is assumed to be a section.

A section should have a corresponding struct within the source code that is default-constructable to be parsed by nlohmann::json.

### usage example

An example how to read and update configuration files can be found within the [examples/config](https://gitlab.com/blrevive/blrevive/-/blob/main/examples/config) directory.

[spdlog]: https://github.com/gabime/spdlog