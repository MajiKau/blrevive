CPMAddPackage(
  NAME nlohmann_json
  URL https://github.com/nlohmann/json/archive/refs/tags/v3.11.2.zip
  VERSION 3.11.2
  OPTIONS "JSON_Install ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
  NAME spdlog
  VERSION 1.8.2
  GITHUB_REPOSITORY gabime/spdlog
  OPTIONS "SPDLOG_INSTALL ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
  NAME semver
  VERSION 0.3.0
  GITHUB_REPOSITORY neargye/semver
  OPTIONS "SEMVER_OPT_INSTALL ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
  NAME magic_enum
  VERSION 0.9.3
  GITHUB_REPOSITORY neargye/magic_enum
  OPTIONS "MAGIC_ENUM_OPT_INSTALL ${BLREVIVE_INSTALL}"  
)

CPMAddPackage(
  NAME BlrSdk
  VERSION 3.2
  GIT_REPOSITORY https://gitlab.com/blrevive/tools/sdk
  GIT_TAG 3.02-re
  OPTIONS "BLRE_SDK_INSTALL ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
  NAME MsDetours
  VERSION 4.0.1
  GIT_REPOSITORY https://github.com/superewald/ms-detours-cmake
  GIT_TAG main
  OPTIONS "MS_DETOURS_INSTALL ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
  NAME SteamworksSDK
  VERSION 1.42.0
  GIT_REPOSITORY https://gitlab.com/superewald/steamworks-sdk
  GIT_TAG main
  OPTIONS "STEAMWORKS_SDK_INSTALL ${BLREVIVE_INSTALL}"
)

CPMAddPackage(
    NAME httplib
    VERSION 0.14.2
    GITHUB_REPOSITORY yhirose/cpp-httplib
    OPTIONS "HTTPLIB_USE_OPENSSL_IF_AVAILABLE off" "HTTPLIB_USE_ZLIB_IF_AVAILABLE off" "HTTPLIB_INSTALL ${BLREVIVE_INSTALL}"
)